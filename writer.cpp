#include <stdio.h>
#include <time.h>

#include "zipstore.h"

class OutFileStream : public ZS::OutStream
 {
  public           :

                   OutFileStream      (const char         *fileName);
  virtual         ~OutFileStream      ();

  virtual void     Write              (const void         *data,
                                       size_t              size);

  virtual unsigned long      GetPos   () const;
  virtual void               Seek     (unsigned long       pos);

  public           :

  FILE            *file;
 };

OutFileStream::OutFileStream          (const char         *fileName)
 {
  file = fopen(fileName,"wb");

  if (file == NULL)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

OutFileStream::~OutFileStream         ()
 {
  if (fclose(file) != 0)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

void
OutFileStream::Write                  (const void         *data,
                                       size_t              size)
 {
  if (fwrite(data,1,size,file) != size)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

unsigned long
OutFileStream::GetPos                 () const
 {
  return ftell(file);
 }

void
OutFileStream::Seek                   (unsigned long       pos)
 {
  if (fseek(file,pos,SEEK_SET) != 0)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

int main (int /*argc*/, char * /*argv*/ [])
 {
  OutFileStream outStream("test.zip");
  ZS::Writer    writer(outStream);

  writer.BeginFile("hello.txt",time(NULL));
  writer.WriteData("hello",5);
  writer.BeginFile("second.txt",time(NULL));
  writer.WriteData("hello, world!!!",15);
  writer.Close();

  return 0;
 }

