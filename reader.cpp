#include <stdio.h>

#include "zipstore.h"

class InFileStream : public ZS::InStream
 {
  public           :

                   InFileStream       (const char         *fileName);
  virtual         ~InFileStream       ();

  virtual void               Read     (void               *dest,
                                       size_t              size);

  virtual unsigned long      GetSize  () const;
  virtual unsigned long      GetPos   () const;
  virtual void               Seek     (unsigned long       pos);

  public           :

  FILE            *file;
 };

InFileStream::InFileStream            (const char         *fileName)
 {
  file = fopen(fileName,"rb");

  if (file == NULL)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

InFileStream::~InFileStream           ()
 {
  if (fclose(file) != 0)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

void
InFileStream::Read                    (void               *dest,
                                       size_t              size)
 {
  if (fread(dest,1,size,file) != size)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

unsigned long
InFileStream::GetSize                 () const
 {
  unsigned long currPos = GetPos();

  if (fseek(file,0,SEEK_END) != 0)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }

  unsigned long size = GetPos();

  if (fseek(file,currPos,SEEK_SET) != 0)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }

  return size;
 }

unsigned long
InFileStream::GetPos                  () const
 {
  return ftell(file);
 }

void
InFileStream::Seek                    (unsigned long       pos)
 {
  if (fseek(file,pos,SEEK_SET) != 0)
   {
    throw ZS::Error(ZS::Error::IO_ERROR);
   }
 }

int main (int /*argc*/, char * /*argv*/ [])
 {
  try
   {
    InFileStream  inStream("test.zip");
    ZS::Reader    reader(inStream);

    while (!reader.IsEOF())
     {
      ZS::Reader::File f = reader.GetFile();

      char datestr[256];
      time_t dateTime = f.GetDateTime();

      strftime(datestr,sizeof(datestr),"%Y.%m.%d %H:%M:%S",localtime(&dateTime));

      printf("File: %s, size: %lu date: %s\n",f.GetName(),f.GetSize(),datestr);

      reader.SeekNext();
     }
   }
  catch (ZS::Error &e)
   {
    fprintf(stderr,"error: %s\n",e.GetMessage());
   }

  return 0;
 }

